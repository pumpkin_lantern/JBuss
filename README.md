# JBuss
JFinal.com 官网分享：http://www.jfinal.com/share/1704

百度搜索：JBuss

#### 介绍
JBuss--为所有jfinal开发者提供二次开发的后台管理系统

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/132226_48f3fbaf_952208.png "F3CMQ6UBY_8Y%2}B`W`5W1U.png")


#### 安装教程

1. 新建数据库loanMarket-develop（mysql8.0），导入sql（loanMarket-develop.sql）
2. 按照文档说明，在jConfig.properties中配置文件上传目录，在tomcat/conf/server.xml配置虚拟目录
3. 将项目导入eclipse或intellij idea 中，配置好即可启动

#### 使用说明

 **1.自动注入** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133634_a4c4d321_952208.png "屏幕截图.png")


 **2.JFinal主配置** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133643_278eec12_952208.png "屏幕截图.png")


 **3.拦截器** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133659_ea6e0272_952208.png "屏幕截图.png")


 **4.伪静态** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133706_1f5d6f4b_952208.png "屏幕截图.png")


 **5.定时任务** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133712_a52c2712_952208.png "屏幕截图.png")


 **6.自定义模板函数** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133730_6c0b141b_952208.png "屏幕截图.png")


 **7.shrio认证，授权，加密算法** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133743_80cdbdcb_952208.png "屏幕截图.png")


 **8.工具类** （含有日期转换，字符处理，doc转pdf和图片html，MD5加密，FTP上传，文件上传，HttpClient，JSON处理，Session共享）

![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133804_f427c9a0_952208.png "屏幕截图.png")

 **9.二次开发代码**  （通过后台配置开发，这里代码不可动）

![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133813_38f09004_952208.png "屏幕截图.png")

 **10.主配置文件** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133818_e70a0a4e_952208.png "屏幕截图.png")

Tomcat/conf/server.xml的新增配置如下:

![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133834_a2abe6c5_952208.png "屏幕截图.png")
注意：如果你的项目存在二级目录（相当于tomcat/webapp/loanMarket-develop）,则需在path中加上该二级目录名（/loanMarket-develop/upload）.
如果项目直至放在tomcat/webapp/ROOT/下，则无需添加二级目录（ROOT）名，直接：path=”/upload”

 **11.前端重点说明插件ajaxFileUpload** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133849_de3608ac_952208.png "屏幕截图.png")

 **12.前端重点说明插件LayPageTool** （该插件由作者结合laypage二次开发）

![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133940_38360b91_952208.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0706/133949_98b6371e_952208.png "屏幕截图.png")

极简部署说明：
已将sql（mysql）文件提供，请创建数据库loanMarket-develop后导入sql，然后直接解压apache-tomcat-8.5.16-.zip，该tomcat已经将项目部署好，请进入web/loanMarket-develop/WEB-INF/classes/jConfig.properties主配置文件中，将数据库名，用户名，密码改为与你本地一致，同时将baseUploadPath
与tomcat/conf/server.xml中的docBase对应到tomcat下的share/upload目录的绝对路径，然后启动tomcat（前提要有jre1.8环境），既可以正常访问。

后台访问地址：localhost:8080/loanMarket-develop/login_.html

用户名：	     superadmin

密码：	     admin

JFinal项目分享：https://www.jfinal.com/share/1704

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 