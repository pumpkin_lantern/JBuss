package com.yuzi.model.systemMo;

import com.jfinal.plugin.activerecord.Db;
import com.yuzi.model.BaseMo.BaseModel;

import java.util.List;

public class SysUserRole extends BaseModel<SysUserRole> {
    //添加权限
    public boolean addSysUserRole(Integer...ids){
        if (isExistSysUserRole(ids))
            return false;

        this.set("user_id",ids[0]);
        this.set("role_id",ids[1]);
        return this.save();
    }

    //做权限判断其是否存在
    public boolean isExistSysUserRole(Integer...ids){
        List<SysUserRole> sysUserRoleList = this.find("select * from sys_user_role where user_id = ? and role_id = ? ",ids[0],ids[1]);
        if (sysUserRoleList != null && sysUserRoleList.size() > 0)
            return true;
        else
            return false;
    }


    //批量删除权限表中的关联关系
    public int deleteSysUserRoleByUserIds(String userIds){
        String sql = "delete from sys_user_role where user_id in"+userIds;
        return Db.update(sql);
    }
    
    
    public int deleteSysUserRoleByUserId(int userId){

        String sql = "delete from sys_user_role where user_id = ?";
        return Db.update(sql,userId);
    }

    public int deleteSysUserRoleByRoleId(int roleId){

        String sql = "delete from sys_user_role where role_id = ?";
        return Db.update(sql,roleId);
    }
}
