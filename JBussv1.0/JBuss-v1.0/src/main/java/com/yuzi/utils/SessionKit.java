package com.yuzi.utils;

import javax.servlet.http.HttpSession;

public class SessionKit {
    private static ThreadLocal<HttpSession > tl = new ThreadLocal<>();

    public static void put(HttpSession s){
        tl.set(s);
    }

    public static HttpSession get(){
      return tl.get();
    }

    public static void remove() {
        tl.remove();
    }
}